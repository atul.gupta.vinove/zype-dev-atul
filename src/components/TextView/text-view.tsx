import React from "react";
import { Text, StyleProp, TextStyle } from "react-native";
import styles from "./styles";

type TextProps = {
	type?: "header" | "subheader";
	children: React.ReactNode
	style?: StyleProp<TextStyle>[];
};

const TextView = ({ type, children, style = [] }: TextProps) => {
	let textStyles: StyleProp<TextStyle>[] = [styles.text];

	if (type === "header") {
		textStyles.push(styles.headerText);
	} else if (type === "subheader") {
		textStyles.push(styles.subHeaderText);
	}
	textStyles = [...textStyles, ...style];

	return <Text style={textStyles}>{children}</Text>;
};

export default TextView