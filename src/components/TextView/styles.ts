import { StyleSheet } from "react-native";
import colors from "../../constants/colors";

const styles = StyleSheet.create({
	text: {
		color: colors.primary,
		fontSize: 18,
		fontFamily: "Poppins-Medium",
	},
	headerText: {
		fontWeight: "600",
		fontSize: 32,
		marginBottom: 12,
	},
	subHeaderText: {
		color: colors.gray,
		fontSize: 20,
		marginBottom: 12,
		marginTop: -12, // assum this shows up under a headerText
	},
});

export default styles;
