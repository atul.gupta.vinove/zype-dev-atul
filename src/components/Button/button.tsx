import React from 'react';
import {
    TouchableOpacity,
    Text,
    StyleProp,
    TextStyle,
    ViewStyle,
} from 'react-native';
import styles from './styles';


type ButtonProps = {
    onPress: () => void;
    title: string;
    type?: 'outline';
};

const Button = ({
    onPress = () => { },
    title = '',
    type,
}: ButtonProps) => {
    const containerStyles: StyleProp<ViewStyle>[] = [styles.container];
    const textStyles: StyleProp<TextStyle>[] = [styles.text];

    if (type === 'outline') {
        containerStyles.push(styles.containerOutline);
        textStyles.push(styles.textOutline);
    }

    return (
        <TouchableOpacity onPress={onPress} style={containerStyles}>
            <Text style={textStyles}>{title}</Text>
        </TouchableOpacity>
    );
};

export default Button