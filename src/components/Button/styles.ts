import { StyleSheet } from "react-native";
import colors from "../../constants/colors";

const styles = StyleSheet.create({
	container: {
		backgroundColor: colors.primary,
		paddingVertical: 14,
		borderRadius: 6,
		borderWidth: 1,
		borderColor: colors.primary,
		marginVertical: 7,
		padding: 5,
	},
	containerOutline: {
		backgroundColor: "transparent",
		borderColor: colors.border,
	},

	text: {
		color: colors.white,
		alignSelf: "center",
		fontSize: 18,
		fontFamily: "Poppins-Regular",
	},
	textOutline: {
		color: colors.primary,
	},
});

export default styles;
