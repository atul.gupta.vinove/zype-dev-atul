import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PrimaryButton from './src/components/Button';
import TextView from './src/components/TextView';

export type Props = {
  name: string;
  baseEnthusiasmLevel?: number;
};

const Hello: React.FC<Props> = ({
  name,
  baseEnthusiasmLevel = 0
}) => {
  const [enthusiasmLevel, setEnthusiasmLevel] = React.useState(
    baseEnthusiasmLevel
  );

  const onIncrement = () =>
    setEnthusiasmLevel(enthusiasmLevel + 1);
  const onDecrement = () =>
    setEnthusiasmLevel(
      enthusiasmLevel > 0 ? enthusiasmLevel - 1 : 0
    );

  const getExclamationMarks = (numChars: number) =>
    numChars > 0 ? Array(numChars + 1).join('!') : '';

  return (
    <View style={styles.container}>
      <TextView>
        Hello {name}
        {getExclamationMarks(enthusiasmLevel)}
      </TextView>
      <View>
        <PrimaryButton
          title='Increase enthusiasm'
          onPress={onIncrement}
          type='outline'
        />
        <PrimaryButton
          title='Decrease enthusiasm'
          onPress={onDecrement}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  greeting: {
    fontSize: 20,
    fontWeight: 'bold',
    margin: 16
  }
});

export default Hello;